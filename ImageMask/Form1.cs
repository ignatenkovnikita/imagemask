﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageMask
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Выберите пожалуйста картину для загрузки";
            openFileDialog1.Filter = "Файл jpg| *.jpg";
            openFileDialog1.FileName = "";
            if (openFileDialog1.ShowDialog() != DialogResult.OK) return;

            pictureBox1.Image = Image.FromFile(openFileDialog1.FileName);
            button2.Enabled = true;
        }

       
        


        private void Form1_Load(object sender, EventArgs e)
        {
            dataGridView1.Rows.Add(3);
            
            //dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            
            
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        private Random random;

        private static Image Invert(Image original)
        {
            Bitmap myImage = new Bitmap(original);
            BitmapData imageData = myImage.LockBits(new Rectangle(0, 0, myImage.Width, myImage.Height), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            int stride = imageData.Stride;
            IntPtr Scan0 = imageData.Scan0;

            unsafe
            {
                byte* p = (byte*)(void*)Scan0;

                int nOffset = stride - myImage.Width * 4;
                int nWidth = myImage.Width;

                for (int y = 0; y < myImage.Height; y++)
                {
                    for (int x = 0; x < nWidth; x++)
                    {
                        p[0] = (byte)(255 - p[0]);
                        p[1] = (byte)(255 - p[1]);
                        p[2] = (byte)(255 - p[2]);
                        p += 4;
                    }
                    p += nOffset;
                }
            }

            myImage.UnlockBits(imageData);
            return (Image)myImage;
        }
        private  Image ChangeColor(Image original)
        {
            Bitmap bmp = new Bitmap(original);

            for (int i = 0; i < (bmp).Width; i++)
            {
                for (int j = 0; j < (bmp).Height; j++)
                {
                    int a = 0;
                    int r = 0;
                    int g = 0;
                    int b = 0;

                    Color clr = bmp.GetPixel(i, j);
                    a = clr.A;
                    r = clr.R;
                    g = clr.G;
                    b = clr.B;

                    r /= trackBar3.Value;
                    g /= trackBar1.Value;
                    b /= trackBar2.Value;

                    Color color = Color.FromArgb(a, r, g, b);

                    bmp.SetPixel(i, j, color);
                }
            }
            //pictureBox2.Refresh();
            return (Image)bmp;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            pictureBox2.Image = Image.FromFile(openFileDialog1.FileName);

            pictureBox2.Image = ChangeColor(pictureBox2.Image);
            //random = new Random();

            
            

            //Color c = Color.Red; //Your desired colour
            //byte r = c.R; //For Red colour
            /*
            Bitmap bmp = pictureBox2.Image as Bitmap;

            for(int i=0; i<(pictureBox2.Image as Bitmap).Width; i++)
            {
                for(int j=0; j<(pictureBox2.Image as Bitmap).Height; j++)
                {
                    int a = 0;
                    int r = 0;
                    int g = 0;
                    int b = 0;

                    Color clr = bmp.GetPixel(i, j);
                    a = clr.A;
                    r = clr.R;
                    g = clr.G;
                    b = clr.B;

                    r /= Convert.ToInt32(dataGridView1.Rows[0].Cells[0].Value); 
                    g /= Convert.ToInt32(dataGridView1.Rows[0].Cells[1].Value);
                    b /= Convert.ToInt32(dataGridView1.Rows[0].Cells[2].Value);

                    Color color = Color.FromArgb(a, r, g, b);

                    bmp.SetPixel(i, j, color); 

                    //bmp.SetPixel(i, j, Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255)));
                    
                }
            }
            pictureBox2.Refresh();
            */
            //Color cc = (pictureBox2.Image as Bitmap).GetPixel(100,100);
            //label1.BackColor = cc;
            //if (cc == Color.Black)
            //    MessageBox.Show("black color");
            

            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            pictureBox2.Image = Invert(pictureBox1.Image);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_MouseClick(object sender, MouseEventArgs e)
        {
            //on mouse click in picture box
            Bitmap bmp = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            pictureBox1.DrawToBitmap(bmp, new Rectangle(Point.Empty, pictureBox1.Size));
            Color pix = bmp.GetPixel(e.X, e.Y);
            MessageBox.Show(string.Format("Red: {0}\nGreen: {1}\nBlue: {2}", pix.R, pix.G, pix.B), "Color");

        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            pictureBox2.Image = Image.FromFile(openFileDialog1.FileName);
            pictureBox2.Image = ChangeColor(pictureBox2.Image);
            //int r = trackBar3.Value;
            //MessageBox.Show(string.Format("Занчение трак 3 {0}",r));
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            pictureBox2.Image = Image.FromFile(openFileDialog1.FileName);
            pictureBox2.Image = ChangeColor(pictureBox2.Image);
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            pictureBox2.Image = Image.FromFile(openFileDialog1.FileName);
            pictureBox2.Image = ChangeColor(pictureBox2.Image);
        }
    }
}
